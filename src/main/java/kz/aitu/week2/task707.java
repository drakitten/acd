package kz.aitu.week2;

import java.util.Scanner;

public class task707
{

        static void printPermutn(String str, String ans)
        {
            if (str.length() == 0) {
                System.out.print(ans + " ");
                return;
            }

            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);

                String ros = str.substring(0, i) +
                        str.substring(i + 1);

                printPermutn(ros, ans + ch);
            }
        }

        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            String a = s.next();
            printPermutn(a, "");
        }

}
