package kz.aitu.week7.sorting;

public class SelectionSort {
    public void sort(int arr[]) {
        int n = arr.length;
        for (int i=0; i<n; i++) {
            int min_ind = i;
            for (int j=i+1; j<n; j++) {
                if (arr[j]<arr[min_ind]) {
                    min_ind = j;
                }
            }
            int temp = arr[min_ind];
            arr[min_ind] = arr[i];
            arr[i] = temp;
        }
    }
}
