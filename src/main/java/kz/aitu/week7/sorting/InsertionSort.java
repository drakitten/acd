package kz.aitu.week7.sorting;

public class InsertionSort {
    public void sort(int arr[]) {
        int n = arr.length;
        for (int i=1; i<n; i++) {
            int a = arr[i];
            int j = i;
            while (j>0 && arr[j-1]>a) {
                arr[j] = arr[j-1];
                j--;
            }
            arr[j] = a;
        }
    }
}

