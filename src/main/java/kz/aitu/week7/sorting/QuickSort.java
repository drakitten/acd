package kz.aitu.week7.sorting;
import java.lang.Math;
public class QuickSort {
    public void sort(int arr[], int low, int high) {
        if (arr.length == 0) {
            return;
        }
        if (low>high) {
            return;
        }
        //int n = (int)Math.random()%arr.length; - if u want u can take random pivot;
        int mid = (high - low)/2 + low;
        int pivot = arr[mid];

        int i = low;
        int j = high;

        while (i<=j) {
            while (arr[i]<pivot) {
                i++;
            }
            while (arr[j]>pivot) {
                j--;
            }
            if (i<=j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (low<j) {
            sort(arr, low, j);
        }
        if (high>i) {
            sort(arr, i, high);
        }
    }
}
