package kz.aitu.week7.sorting;

public class MergeSort {
    public void sort(int arr[], int left, int right) {
        if (arr.length == 0) {
            return;
        }
        if (left<right) {
            int mid = (right+left)/2;
            sort(arr, left, mid);
            sort(arr, mid+1, right);
            merge(arr, left, mid, right);
        }
    }
    public void merge(int arr[], int left, int mid, int right) {
        int l1 = mid - left + 1;
        int l2 = right - mid;

        int a1[] = new int[l1];
        int a2[] = new int[l2];

        for (int i=0; i<l1; i++) {
            a1[i] = arr[left + i];
        }
        for (int j=0; j<l2; j++) {
            a2[j] = arr[mid + 1 + j];
        }

        int i = 0;
        int j = 0;

        int k = left;
        while (i<l1 && j<l2){
            if (a1[i]>a2[j]) {
                arr[k] = a2[j];
                j++;
            } else {
                arr[k] = a1[i];
                i++;
            }
            k++;
        }
        while (i<l1) {
            arr[k] = a1[i];
            i++;
            k++;
        }
        while (j<l2) {
            arr[k] = a2[j];
            j++;
            k++;
        }
    }
}
