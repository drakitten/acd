package kz.aitu.week7.sorting;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int arr[] = {7, 1, 3, 2, 9};
        //System.out.println("Insertion sort");
        //InsertionSort is = new InsertionSort();
        //is.sort(arr);

        //System.out.println("Bubble sort");
        //BubbleSort bs = new BubbleSort();
        //bs.sort(arr);

        //System.out.println("Quick sort");
        //QuickSort qs = new QuickSort();
        //qs.sort(arr, 0, arr.length-1);

        //System.out.println("Merge sort");
        //MergeSort ms = new MergeSort();
        //ms.sort(arr, 0, arr.length-1);

        System.out.println("Selection sort");
        SelectionSort ss = new SelectionSort();
        ss.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
