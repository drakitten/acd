package kz.aitu.week1.quiz1;

import java.util.Scanner;

public class task1 {

    public static void task_1(int n) {
        for (int i=1; i<=n; i++) {
            System.out.println(i);
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        task_1(n);
    }
}

