package kz.aitu.week1.quiz1;

import java.util.Scanner;

public class task4 {
    public static String task(String s){
        if(s.length() == 1){
            System.out.println("YES");
        }
        if(s.substring(0, 1).equals(s.substring(s.length() - 1, s.length()))){
            if(s.length() == 2){
                return "YES";
            }
            return task(s.substring(1, s.length() - 1));
        }else{
            return "NO";
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String word;
        word = s.nextLine();

        System.out.println(task(word));
    }
}
