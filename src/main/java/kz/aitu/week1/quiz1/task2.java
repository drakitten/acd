package kz.aitu.week1.quiz1;

import java.util.Scanner;

public class task2 {

    public static int maxn() {
        Scanner s = new Scanner(System.in);
        int k = s.nextInt();
        if (k==0) {
            return 0;
        }
        int m = maxn();
        if (k>m) return k;
        else return m;

    }
    public static void main(String[] args) {
        System.out.println(maxn());

    }
}
