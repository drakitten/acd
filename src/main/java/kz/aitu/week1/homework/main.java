import java.util.Scanner;

public class main {

    public static int task1(int n, int arr[]) {
        int minm = arr[0];
        for (int i = 1; i<n; i++) {
            if (arr[i]<minm) {
                minm = arr[i];
            }
        }
        return minm;
    }
    public static int task2(int n, int arr[]) {
        int summ = 0;
        for (int i=0; i<n; i++) {
            summ+=arr[i];
        }
        return summ/n;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int arr[] = new int[100];
        for (int i = 0; i<n; i++) {
            arr[i] = s.nextInt();
        }

        System.out.println(task1(n, arr));
        System.out.println(task2(n, arr));

    }
}
