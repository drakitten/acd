import java.util.Scanner;

public class task9 {
    public static int task(int n, int k) {
        if (n==k || k==0) return 1;
        return task(n-1, k-1)+task(n-1, k);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int k = s.nextInt();
        System.out.println(task(n, k));
    }
}
