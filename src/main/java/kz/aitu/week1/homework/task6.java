import java.util.Scanner;

public class task6 {
    public static int task(int a, int n) {
        if (n==1) return a;
        return task(a, n-1)*a;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int n = s.nextInt();
        System.out.println(task(a,n));
    }
}
