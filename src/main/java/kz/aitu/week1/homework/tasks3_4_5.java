import java.util.Scanner;

public class tasks3_4_5 {
    public static void task3(int n) {
        int a = 0;
        for (int i=n-1; i>1; i--) {
            if(n%i==0) a++;
        }
        if (a>0) System.out.println("Composite");
        else System.out.println("Prime");
    }
    public static int task4(int n) {
        if (n==1) return 1;
        return n*task4(n-1);
    }
    public static int task5(int n) {
        if (n==1 || n==0) return 1;
        return task5(n-1)+task5(n-2);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        task3(n);
        System.out.println(task4(n));
        System.out.println(task5(n));
    }
}
