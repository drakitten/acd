import java.util.Scanner;

public class task10 {
    public static int gcd(int a, int b){
        if (a==0) return b;
        return gcd(b%a, a);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();
        System.out.println(gcd(a, b));
    }
}
