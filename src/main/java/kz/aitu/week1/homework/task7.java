import com.sun.org.apache.xpath.internal.objects.XString;

import java.util.Scanner;

public class task7 {
    public static String task(int n, int arr[]) {
        if (n==1) return arr[n-1]+" ";
        return arr[n-1]+" "+task(n-1, arr);

    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i<n; i++) {
            arr[i] = s.nextInt();
        }

        System.out.println(task(n, arr));
    }
}
