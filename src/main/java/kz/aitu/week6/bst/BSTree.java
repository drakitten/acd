package kz.aitu.week6.bst;

public class BSTree {
    private Node root;

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        if(root == null) {
            root = node;
            return;
        } else {
            Node current = root;
            Node parent = null;
            while (true) {
                parent = current;
                if (key < current.getKey()) {
                    current = current.getLeft();
                    if (current == null) {
                        parent.setLeft(node);
                        return;
                    }
                } else {
                    current = current.getRight();
                    if (current == null) {
                        parent.setRight(node);
                        return;
                    }
                }
            }
        }
    }

    public boolean delete(int key){
        Node parent = root;
        Node current = root;
        boolean left = false;
        while(current.getKey() != key){
            parent = current;
            if(current.getKey() > key){
                left = true;
                current = current.getLeft();
            } else {
                left = false;
                current = current.getRight();
            }
            if(current == null){
                return false;
            }
        }
        if(current.getLeft() == null && current.getRight() == null){
            if(current==root){
                root = null;
            }
            if(left){
                parent.setLeft(null);
            }else{
                parent.setRight(null);
            }
        } else if(current.getRight() == null){
            if(current == root){
                root = current.getLeft();
            } else if(left){
                parent.setLeft(current.getLeft());
            } else{
                parent.setRight(current.getLeft());
            }
        } else if(current.getLeft() == null){
            if(current == root){
                root = current.getRight();
            } else if(left){
                parent.setLeft(current.getRight());
            } else{
                parent.setRight(current.getRight());
            }
        } else if(current.getLeft() != null && current.getRight() != null){
            Node successor	 = getSuccessor(current);
            if(current == root){
                root = successor;
            } else if(left){
                parent.setLeft(successor);
            } else {
                parent.setRight(successor);
            }
            successor.setRight(current.getRight());
        }
        return true;
    }

    public Node getSuccessor(Node del){
        Node successor = null;
        Node successorParent = null;
        Node current = del.getLeft();
        while(current != null){
            successorParent = successor;
            successor = current;
            current = current.getRight();
        }
        if(successor != del.getRight()){
            successorParent.setRight(successor.getLeft());
            successor.setLeft(del.getLeft());
        }
        return successor;
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        printNodeAs(root);
        System.out.println();
    }

    public void printAll() {
        for (int i=1; i<=5; i++) {
            printNode(root, i);
        }
        System.out.println();
    }

    private void printNode(Node root, int a) {
        if (root == null) return;
        if (a == 1) {
            System.out.print(root.getValue());
        } else if (a > 1) {
            printNode(root.getLeft(),a-1);
            printNode(root.getRight(),a-1);
        }
    }
    private void printNodeAs(Node root) {
        if(root == null) return;
        printNodeAs(root.getLeft());
        System.out.print(root.getValue());
        printNodeAs(root.getRight());
    }
}
