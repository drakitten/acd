package kz.aitu.week6.binarytree;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree b = new BinarySearchTree();
        b.insert(5);
        b.insert(2);
        b.insert(7);
        b.insert(4);
        b.insert(3);
        b.insert(8);
        b.insert(6);
        b.display(b.root);
    }
}
