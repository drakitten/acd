package kz.aitu.week6.binarytree;

public class BinarySearchTree {
    Node root;

    public BinarySearchTree() {
        this.root = null;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public boolean find(int n) {
        Node current = root;
        while (current != null) {
            if (current.data == n) {
                return true;
            } else if (current.data > n) {
                current = current.left;
            } else {
                current = current.right;
            }
        }
        return false;
    }

    public void insert(int n){
        Node newNode = new Node(n);
        if(root==null) {
            root = newNode;
            return;
        }
        Node current = root;
        Node parent = null;
        while(true) {
            parent = current;
            if(n < current.data){
                current = current.left;
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            } else {
                current = current.right;
                if(current == null) {
                    parent.right = newNode;
                    return;
                }
            }
        }
    }

    public void display(Node root) {
        if (root != null) {
            display(root.left);
            System.out.print(" " + root.data);
            display(root.right);
        }
    }

    public boolean delete(int n) {
        Node current = root;
        Node parent = root;
        boolean left = false;
        while (current.data != n) {
            if (current.data > n) {
                current = current.left;
                left = true;
            } else {
                current = current.right;
                left = false;
            }
            if (current == null) {
                return false;
            }
        }
        //current = id
        if (current.left == null && current.right == null) {
            if (current == root) {
                root = null;
            }
            if (left) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (current.right == null) {
            if (current == root) {
                root = current.left;
            }
            if (left) {
                parent.left = current.left;
            } else {
                parent.right = current.left;
            }
        } else if (current.left == null) {
            if (current == root) {
                root = current.right;
            }
            if (left) {
                parent.left = current.right;
            } else {
                parent.right = current.right;
            }
        } else if (current.left != null && current.right != null) {
            Node successor = getSuccessor(current);
            if (current == root) {
                root = successor;
            } else if (left) {
                parent.left = successor;
            } else {
                parent.right = successor;
            }
            successor.left = current.left;
        }
        return true;
    }

    public Node getSuccessor(Node del) {
        Node successsor = null;
        Node successsorParent = null;
        Node current = del.right;
        while (current != null) {
            successsorParent = successsor;
            successsor = current;
            current = current.left;
        }
        if (successsor != del.right) {
            successsorParent.left = successsor.right;
            successsor.right = del.right;
        }
        return successsor;
    }

}
