package kz.aitu.week6.bonusP;

public class LinkedList {
    private Node head;
    private Node tail;
    int lvlpoint = 0;

    public void addParent(String name) {
        Node new_par = new Node(new NodeSecond(name, 80, null));
        if(head == null) {
            head = new_par;
        } else {
            tail.setNext(new_par);
        }
        tail = new_par;
    }

    public void addChild(String child, String parent){
        Node current = head;
        addChildN(current, child, parent);
    }

    private void addChildN(Node first, String child, String parent){
        if (first!=null){
            NodeSecond node = first.getNode();
            addChildSec(node, child, parent);
            addChildN(first.getNext(), child, parent);
        }
    }
    private void addChildSec(NodeSecond root, String child, String parent){
        if (root != null) {
            if (root.getName() == parent) {
                if (root.getLeft() == null) {
                    root.setLeft(new NodeSecond(child, 80, root));
                    lvlPoint(root.getLeft());
                } else if (root.getRight() == null) {
                    root.setRight(new NodeSecond(child, 80, root));
                    lvlPoint(root.getRight());
                } else {
                    System.out.println("Error: Too Many Children");
                }
            }
            addChildSec(root.getLeft(), child, parent);
            addChildSec(root.getRight(), child, parent);
        }
    }

    public void lvlPoint(NodeSecond root) {
        int lvlpoint = 5;
        while (root.getParent() != null) {
            root.getParent().setPoint(root.getParent().getPoint()+lvlpoint);
            root = root.getParent();
            if (lvlpoint != 1) {
                lvlpoint -= 2;
            }
        }
    }
    public int getPoints(String name) {
        Node current = head;
        calcNode(current, name);
        int points = lvlpoint;
        lvlpoint = 0;
        return points;
    }

    private void calcNode(Node current, String name){
        if (current != null){
            NodeSecond node = current.getNode();
            calcNodeS(node, name);
            calcNode(current.getNext(), name);
        }
    }

    private void calcNodeS(NodeSecond root, String name){
        if (root!=null) {
            if (root.getName() == name) {
                lvlpoint = root.getPoint();
            }
            calcNodeS(root.getLeft(), name);
            calcNodeS(root.getRight(), name);
        }
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }
}
