package kz.aitu.week6.bonusP;

public class NodeSecond {
    private NodeSecond parent;
    private String name;
    private NodeSecond left;
    private NodeSecond right;
    private int point;
    private int depth;


    public NodeSecond(String name, int point, NodeSecond parent) {
        this.name = name;
        this.parent = parent;
        this.point = point;
        this.left = null;
        this.right = null;
    }

    public int getPoint() {
        return point;
    }

    //i didnt use it..
    public String getName() {
        return name;
    }

    public void setRight(NodeSecond right) {
        this.right = right;
    }

    public void setLeft(NodeSecond left) {
        this.left = left;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent(NodeSecond parent) {
        this.parent = parent;
    }

    public NodeSecond getLeft() {
        return left;
    }

    public NodeSecond getParent() {
        return parent;
    }

    public NodeSecond getRight() {
        return right;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
