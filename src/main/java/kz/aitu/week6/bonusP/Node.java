package kz.aitu.week6.bonusP;

public class Node {
    private NodeSecond node;
    private Node next;

    public Node(NodeSecond node) {
        this.node = node;
        this.next = null;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public void setNode(NodeSecond node) {
        this.node = node;
    }

    public NodeSecond getNode() {
        return node;
    }

    public Node getNext() {
        return next;
    }
}
