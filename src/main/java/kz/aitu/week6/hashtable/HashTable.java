package kz.aitu.week6.hashtable;

import java.util.Arrays;
import java.util.Objects;

public class HashTable extends Object{
    private int size = 10;
    private Node array[] = new Node[size];
    private double loadFactor = 0.75;
    private int initialCapacity = 0;

    public void put(int key, String value){
        Node node = new Node(key, value);
        int index = node.hashCode()%size;
        if (array[index] == null) {
            array[index] = node;
        } else {
            Node current = array[index];
            while (current != null) {
                if (current.getKey() == node.getKey()){
                    current.setValue(node.getValue());
                    return;
                }
                current = current.getNext();
            }
            current = node;
        }
        initialCapacity++;
        if(1.0*initialCapacity/size >= 0.7){
            loadFactor*=2;
            size = 0;
            //shuffle array after everything = null;
        }
    }

    public void get(int key){
        for (int i=0; i<size; i++) {
            Node new_node = array[i];
            while(new_node != null) {
                if(new_node.getKey() == key) {
                    System.out.println(new_node.getValue());
                    return;
                }
                new_node = new_node.getNext();
            }
        }
    }

    public void print() {
        for(int i=0; i<size; i++) {
            Node current = array[i];
            while(current != null){
                System.out.print(current.getValue() + " ");
                current = current.getNext();
            }
        }
        System.out.println();
    }

    public void remove(int key) {
        for (int i=0; i<size; i++) {
            Node new_node = array[i];
            while(new_node != null) {
                if (new_node.getKey() == key) {
                    if(new_node.getNext() != null) {
                        new_node = new_node.getNext().getNext();
                    }
                    return;
                }
            }
        }
    }

    public double getLoadFactor() {
        return loadFactor;
    }

    public int getInitialCapacity() {
        return initialCapacity;
    }

    public void setLoadFactor(double loadFactor) {
        this.loadFactor = loadFactor;
    }

    public void setInitialCapacity(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}