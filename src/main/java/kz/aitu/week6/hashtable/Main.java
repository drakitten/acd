package kz.aitu.week6.hashtable;

public class Main {
    public static void main(String[] args) {
        HashTable hs = new HashTable();
        hs.put(1, "Ven");
        hs.put(2, "Bek");
        System.out.println("Printing hs: ");
        hs.print();
        System.out.println("Getting by key 2: ");
        hs.get(2);
        hs.remove(2);
        System.out.println("Printing after deleting: ");
        hs.print();
        //remove doesn't work properly...
    }
}
