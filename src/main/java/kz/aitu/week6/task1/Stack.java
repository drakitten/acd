package kz.aitu.week6.task1;

public class Stack {
    Node top;

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void pop(){
        Node pop = top;
        top = top.next;
    }

    public void push(int value) {
        Node new_node = new Node(value);
        if (top == null) {
            top = new_node;
        } else {
            new_node.next = top;
            top = new_node;
        }
    }
    public Stack(){
        this.top = new Node(0);
    }

    public Node top(){
        return top;
    }


}
