package kz.aitu.week6.task1;

public class Main {
    public static void main(String[] args) {
        Stack st1 = new Stack();
        Stack st2 = new Stack();
        Stack st3 = new Stack();

        st1.push(1);
        st1.push(2);

        st2.push(4);
        st2.push(6);

        st3.push(5);
        st3.push(6);


        Stack st4 = new Stack();
        st4.top = merge(st1.top, st2.top, st3.top);

        System.out.print("printing stack ");
        Node current = st4.top;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }
    }

    public static Node merge(Node headA, Node headB, Node headC) {
        Node res = new Node(0);
        Node tail = res;
        while(true) {
            if(headA == null && headB == null) {
                tail.next = headC;
                break;
            }
            if(headB == null && headC == null) {
                tail.next = headA;
                break;
            }
            if(headC == null && headA == null) {
                tail.next = headB;
                break;
            }
            if(headA.value <= headB.value && headA.value <= headC.value) {
                tail.next = headA;
                headA = headA.next;
            } else if (headB.value <= headA.value && headB.value <= headC.value){
                tail.next = headB;
                headB = headB.next;
            } else {
                tail.next = headC;
                headC = headC.next;
            }
            tail = tail.next;
        }
        return res.next;
    }

}
