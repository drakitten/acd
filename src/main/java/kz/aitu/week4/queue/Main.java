package kz.aitu.week4.queue;

public class Main {
    public static void main(String[] args) {
        Queue qu = new Queue();
        qu.add(1);
        qu.add(2);
        qu.add(3);
        qu.poll();
        qu.remove();
        System.out.println("size is " + qu.size());
        System.out.println("peek " + qu.peek());
        System.out.print("printing queue: ");
        Node current = qu.head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();

    }
}
