package kz.aitu.week4.queue;

import lombok.Data;

@Data

public class Queue {
    Node head;
    Node tail;

    public void add(int data) {
        Node new_node = new Node(data);
        if (tail == null) {
            head = tail = new_node;
        } else {
            tail.next = new_node;
            new_node.next = null;
            tail = new_node;
        }
    }
    public int peek() {
        if (head == null) {
            return -1;
        } else{
            return head.data;
        }
    }
    public void remove() {
        head = head.next;
    }
    public Node poll() {
        Node old_head = head;
        if (head == null) {
            return null;
        } else {
            head = head.next;
            return old_head;
        }
    }
    public int size(){
        Node current = head;
        int i = 0;
        while (current != null) {
            current = current.next;
            i++;
        }
        return i;
    }
}
