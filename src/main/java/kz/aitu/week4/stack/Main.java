package kz.aitu.week4.stack;

public class Main {
    public static void main(String[] args) {
        Stack st = new Stack();
        st.push(1);
        st.push(2);
        st.push(3);

        System.out.print("printing stack ");
        Node current = st.top;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();

        System.out.println("popped " + st.pop());
        System.out.println("is stack empty? " + st.empty());
        System.out.println("size of stack " + st.size());

        System.out.print("printing stack after changes ");
        Node current2 = st.top;
        while (current2 != null) {
            System.out.print(current2.data + " ");
            current2 = current2.next;
        }
        System.out.println();
    }
}
