package kz.aitu.week4.stack;

import lombok.Data;

@Data
public class Stack {
    Node top;

    public int pop(){
        Node popped;
        popped = top;
        top = top.next;
        return popped.data;
    }

    public void push(int data) {
        Node new_node = new Node(data);
        if (top == null) {
            top = new_node;
        } else {
            new_node.next = top;
            top = new_node;
        }
    }

    public boolean empty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        Node current = top;
        int i=0;
        if (top == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                i++;
            }
        }
        return i;
    }

    public int top() {
        if (top == null) {
            return 0;
        } else {
            return top.data;
        }
    }
}
