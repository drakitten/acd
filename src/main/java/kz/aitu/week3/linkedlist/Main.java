package kz.aitu.week3.linkedlist;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        LinkedList ls = new LinkedList();
        Block b = ls.getHead();

        Scanner s = new Scanner(System.in);

        for (int i=0; i<10 ;i++) {
            int a = s.nextInt();
            ls.addBlock(new Block(a));
        }

        ls.printData();
        System.out.print("\n"+ls.size());
    }
}

