package kz.aitu.week3.linkedlist;

public class LinkedList {
    private Block head;
    private Block tail;

    public void addBlock(Block block){
        if (head == null){
            head = block;
            tail = block;
        } else{
            tail.setNextBlock(block);
            tail = block;
        }
    }

    public void printData() {
        Block block = getHead();
        while (block!=null) {
            System.out.print(block.getValue()+" ");
            block = block.getNextBlock();
        }
    }
    public int size() {
        int count=0;
        Block block = getHead();
        while (block!=null) {
            block = block.getNextBlock();
            count++;
        }
        return count;
    }
    public void setHead(Block head) {
        this.head = head;
    }
    public void setTail(Block tail) {
        this.tail = tail;
    }
    public Block getHead() {
        return head;
    }
    public Block getTail() {
        return tail;
    }
}
