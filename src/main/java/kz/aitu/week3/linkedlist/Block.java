package kz.aitu.week3.linkedlist;

public class Block {
    private int value;
    private Block nextBlock;

    public int getValue(){
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Block(int value) {
        this.value = value;
    }

    public Block getNextBlock() {
        return nextBlock;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }
}
