package kz.aitu.week3.real_quiz;

public class LinkedList{ //nothing to change
    private Node head;
    private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {
        //write your code here
        Node new_node = new Node(s);
        Node current = head;
        int i = 0;
        while (i < index) {
            current = current.next;
            i++;
        }
        new_node.next = current.next;
        current.next = new_node;
    }

    public void removeAt(int index) {
        //write your code here
        Node current = head;
        int i = 0;
        while (i < index) {
            current = current.next;
            i++;
        }
        Node del_node = current.next;
        current.next = del_node.next;
    }
}
