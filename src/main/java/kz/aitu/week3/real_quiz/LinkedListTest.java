package kz.aitu.week3.real_quiz;

public class LinkedListTest {

    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        linkedList.insertAt("CS1902", 2);
        linkedList.removeAt(1);
        linkedList.removeAt(0);

        int length = 0;
        Node middle = linkedList.head();
        Node current = linkedList.head();

        while (current.next != null) {
            if (length%2==0) {
                middle = middle.next;
            }
            length++;
            current = current.next;
        }
        if (length%2==0) {
            middle = middle.next;
        }
        Node a = linkedList.head();
        System.out.print("Changed list:  ");
        for (int i=0; i<4; i++) {
            System.out.print(a+" ");
            a=a.next;
        }
        System.out.println();
        System.out.println("Middle is equal to " + middle);

    }

}
