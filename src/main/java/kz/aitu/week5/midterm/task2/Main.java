package kz.aitu.week5.midterm.task2;

public class Main {
    static Stack tmp = new Stack();
    static Stack st3 = new Stack();

    public static void main(String[] args) {
        Stack st1 = new Stack();
        Stack st2 = new Stack();
        st1.push(1);
        st1.push(2);
        st1.push(3);
        st1.push(4);
        st2.push(2);
        st2.push(6);
        st2.push(7);

        mergeStack(st1, st2);

        System.out.print("printing stack ");
        while (tmp.size() != 0) {
            System.out.print(tmp.top() + " ");
            tmp.pop();
        }
    }
    /*O(n+m): two inputs as two variables*/
    public static void mergeStack(Stack st1, Stack st2){
        int first = st1.size();
        int second  = st2.size();
        while(first != 0) {
            st3.push(st1.top());
            st1.pop();
        }
        while(second != 0) {
            st3.push(st2.top());
            st1.pop();
        }
        sortStack(st3);
    }
    /*O(n)*/
    public static void sortStack(Stack st3){
        while (st3.size() != 0)
        {
            int current = st3.top();
            st3.pop();
            while (tmp.size() != 0 && tmp.top() > current) {
                st3.push(tmp.top());
                tmp.pop();
            }
            tmp.push(current);
        }
    }
}
