package kz.aitu.week5.midterm.task2;

public class Stack {
    Node top;

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }
    /*O(1)*/
    public void pop() {
        Node popped;
        popped = top;
        top = top.next;
    }
    /*O(1)*/
    public void push(int value) {
        Node new_node = new Node(value);
        if (top == null) {
            top = new_node;
        } else {
            new_node.next = top;
            top = new_node;
        }
    }
    /*O(n)*/
    public int size() {
        Node current = top;
        int i=0;
        if (top == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                i++;
            }
        }
        return i;
    }
    /*O(1)*/
    public int top() {
        if (top == null) {
            return 0;
        } else {
            return top.value;
        }
    }

}
