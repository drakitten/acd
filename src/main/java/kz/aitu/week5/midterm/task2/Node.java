package kz.aitu.week5.midterm.task2;

public class Node {
    int value;
    Node next;

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node(int value){
        this.value = value;
    }
}
