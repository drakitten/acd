package kz.aitu.week5.midterm.task3;

public class Queue {
    Node head;
    Node tail;

    public void add(String value) {
        Node new_node = new Node(value);
        if (tail == null) {
            head = tail = new_node;
        } else {
            tail.next = new_node;
            new_node.next = null;
            tail = new_node;
        }
    }
    public String peek() {
        if (head == null) {
            return "-1";
        } else{
            return head.value;
        }
    }
    public void remove() {
        head = head.next;
    }
    public Node poll() {
        Node old_head = head;
        if (head == null) {
            return null;
        } else {
            head = head.next;
            return old_head;
        }
    }
    public int size(){
        Node current = head;
        int i = 0;
        while (current != null) {
            current = current.next;
            i++;
        }
        return i;
    }
    /*O(n)*/
    public void print(){
        Node current = head;
        while(current != null){
            if(current.value.size()%2 != 0) {
                System.out.println();
            }
            current = current.next;
        }
    }
}
