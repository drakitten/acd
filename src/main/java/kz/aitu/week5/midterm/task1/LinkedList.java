package kz.aitu.week5.midterm.task1;

public class LinkedList {
    Node head;
    Node tail;

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }
    /*bigO: O(n), because we iterate from the very beginning to the end*/
    public void push_back(int value){
        Node new_node = new Node(value);
        if (head == null){
            head = new_node;
            tail = new_node;
        } else{
            tail.setNext(new_node);
            tail = new_node;
        }
    }
    /*bigO: O(1) printing and iterating from top to end*/
    public int print(){
        System.out.print(head.getValue()+" ");
        head = head.getNext();

        if(head == null){
            return 1;
        }
        return print();
    }
}
