package kz.aitu.week9.bfs;

public class Main {
    public static void bfs(BinaryTree t){
        if (t.getRoot() == null) {
            return;
        }
        Queue q = new Queue();
        q.add(t.getRoot().getData());
        while(!q.isEmpty()) {
            Node temp = q.poll();
            System.out.print(temp.getData() + " ");
            BinaryTreeNode b = t.find(temp.getData());
            if (b.getLeft() != null) {
                q.add(b.getLeft().getData());
            }
            if (b.getRight() != null) {
                q.add(b.getRight().getData());
            }
        }
    }

    public static void main(String[] args) {
        BinaryTree t = new BinaryTree();
        t.insert(1);
        t.insert(2);
        t.insert(3);
        t.insert(4);
        t.insert(5);

        bfs(t);
    }
}
