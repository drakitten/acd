package kz.aitu.week9.bfs;

public class BinaryTree {
    public BinaryTreeNode root;

    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }

    public BinaryTreeNode getRoot() {
        return root;
    }

    public void insert(int n) {
        BinaryTreeNode newNode = new BinaryTreeNode(n);
        if(root == null) {
            root = newNode;
            return;
        }
        BinaryTreeNode current = root;
        BinaryTreeNode parent = null;
        while(true) {
            parent = current;
            if(n < current.data){
                current = current.left;
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            } else {
                current = current.right;
                if(current == null) {
                    parent.right = newNode;
                    return;
                }
            }
        }
    }

    public void display(BinaryTreeNode root) {
        if (root != null) {
            display(root.left);
            System.out.print(" " + root.data);
            display(root.right);
        }
    }
    public BinaryTreeNode find(int data) {
        return findNode(this.root, data);
    }
    private BinaryTreeNode findNode(BinaryTreeNode b, int ser) {
        if (b == null) {
            return null;
        }
        if (ser > b.data) {
            return findNode(b.right, ser);
        } else if (ser < b.data) {
            return findNode(b.left, ser);
        } else {
            return b;
        }
    }



}
