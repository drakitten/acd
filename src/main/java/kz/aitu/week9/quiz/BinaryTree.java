package kz.aitu.week9.quiz;

public class BinaryTree {
    private Node root;

    public BinaryTree() {
        this.root = null;
    }

    public void insert(int key) {
        Node node = new Node(key);
        if (root == null) {
            root = node;
            return;
        } else {
            Node current = root;
            Node parent = null;
            while (current != null) {
                parent = current;
                if (current.getKey() < key) {
                    current = current.getLeft();
                    if (current == null) {
                        parent.setLeft(node);
                    }
                } else {
                    current = current.getRight();
                    if (current == null) {
                        parent.setRight(node);
                    }
                }
            }
        }
    }

    public int getHeight() {
        int max1 = 0;
        int max2 = 0;
        if (root == null) {
            return 0;
        }
        Node current1 = root;
        Node current2 = root;
        if (current1.getLeft()!=null) {
            max1+=getHeightNum(current1.getLeft(), max1);
        }
        if (current1.getRight()!=null) {
            max2+=getHeightNum(current1.getRight(), max2);
        }

        return Math.max(max1+1, max2+1);
    }
    public int getHeightNum(Node node, int cnt) {
        if (node.getLeft()!=null) {
            cnt++;
            getHeightNum(node.getLeft(), cnt);
        }
        if (node.getRight()!=null) {
            cnt++;
            getHeightNum(node.getRight(), cnt);
        }
        return cnt;
    }

    public int totalSum(Node node) {
        if (root == null) return 0;
        int sum = 0;
        Node current = root;
        sum+=totalSumNum(current.getLeft(), sum);
        sum+=totalSumNum(current.getRight(), sum);

        return sum+root.getKey()-1;
    }
    public int totalSumNum(Node node, int sum) {
        if (node != null) {
            sum += node.getKey();
        }
        if (node.getRight() != null) {
            return totalSumNum(node.getRight(), sum);
        }
        if (node.getLeft() != null) {
            return totalSumNum(node.getLeft(), sum);
        }
        return sum;
    }

    public int findMax() {
        if (root == null) return 0;
        int max = root.getKey();
        Node current = root;
        if (current.getLeft() == null) {
            return root.getKey();
        } else {
            while (current.getLeft() != null) {
                current = current.getLeft();
            }
        }
        return current.getKey();
    }

    public void printAll(Node node) {
        if (node != null) {
            printAll(node.getRight());
            System.out.print(node.getKey()+" ");
            printAll(node.getLeft());
        }
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public Node getRoot() {
        return root;
    }
}
