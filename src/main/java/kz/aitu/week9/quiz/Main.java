package kz.aitu.week9.quiz;

public class Main {
    public static void main(String[] args) {
        BinaryTree b = new BinaryTree();
        b.insert(4);
        b.insert(2);
        b.insert(6);
        b.insert(1);
        b.insert(3);
        b.insert(5);
        b.insert(7);

        b.printAll(b.getRoot());
        System.out.println();
        System.out.println(b.findMax());
        System.out.println(b.totalSum(b.getRoot()));
        System.out.println(b.getHeight());
    }
}
