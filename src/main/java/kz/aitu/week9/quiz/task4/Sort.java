package kz.aitu.week9.quiz.task4;

import java.util.Arrays;

public class Sort {
    public void isAnagram(String s1, String s2) {
        String s3 = sort(s1, 0, s1.length() - 1);
        String s4 = sort(s2, 0, s2.length()-1);
        System.out.println(s3+" "+s4);
        if (s3.equals(s4)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
    public String sort(String s, int l, int r) {
        if (s.length() == 0) {
            return s;
        }
        if (l>r) {
            return s;
        }
        char[] a = s.toCharArray();
        int mid = (r-l)/2+l;
        char pivot = a[mid];
        int i=l;
        int j=r;
        while(i<=j) {
            while (a[i]<pivot) {
                i++;
            }
            while (a[j]>pivot) {
                j--;
            }
            if (i<=j) {
                char temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
            }
        }
        if (l<j) {
            String str = sort(s, l, j);
        }
        if (r>i) {
            String str = sort(s, i, r);
        }
        s = Arrays.toString(a);
        return s;
    }
}
