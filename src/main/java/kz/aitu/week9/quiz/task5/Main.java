package kz.aitu.week9.quiz.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Search search = new Search();
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] arr = new int[size];
        for (int i=0; i<size; i++) arr[i] = s.nextInt();
        int num = s.nextInt();
        search.bin(arr, num, size);
    }
}
