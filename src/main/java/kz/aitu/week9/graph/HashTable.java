package kz.aitu.week9.graph;

public class HashTable<Key, Value> {
    public Key key;
    public Vertex array[];
    public int size = 10;

    public HashTable() {
        this.array = new Vertex[size];
    }

    public boolean containsKey(Key key){
        int index = key.hashCode() % 10;
        if(array[index] != null){
            Vertex current = array[index];
            while (current != null) {
                if (current.getKey() == key) {
                    return true;
                }
                current = current.getNext();
            }
        }
        return false;
    }

    public Vertex get(Key key) {
        int index = key.hashCode() % size;
        if (array[index] != null) {
            Vertex current = array[index];
            while (current != null) {
                if (current.getKey() == key) {
                    return current;
                }
                current = current.getNext();
            }
        }
        return null;
    }

    public int countVertices() {
        int cnt = 0;
        for (int i = 0; i<size; i++) {
            Vertex current = array[i];
            while (current != null) {
                cnt++;
                current = current.getNext();
            }
        }
        return cnt;
    }

    public void put(Key key, Vertex vertex) {
        int index = key.hashCode() % size;
        if (array[index] == null) {
            array[index] = vertex;
            return;
        } else {
            Vertex current = array[index];
            while (current != null) {
                if (current.getKey() == key) {
                    current = vertex;
                }
                current = current.getNext();
            }
            if (current == null) {
                current = vertex;
            }

        }
    }
    public void printAll() {
        for (int i = 0; i<size; i++) {
            Vertex current = array[i];
            while (current != null) {
                System.out.print(current.getValue() + " ");
                current = current.getNext();
            }
        }
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setArray(Vertex[] array) {
        this.array = array;
    }

    public Key getKey() {
        return key;
    }

    public Vertex[] getArray() {
        return array;
    }

    public int getSize() {
        return size;
    }
}
