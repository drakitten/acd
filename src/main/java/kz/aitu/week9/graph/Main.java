package kz.aitu.week9.graph;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer, String>();

        graph.addVertex(1, "value");
        graph.addVertex(2, "value");
        graph.addEdge(1, 2);

        graph.addVertex(3, "value");

        graph.printAll();
    }
}
