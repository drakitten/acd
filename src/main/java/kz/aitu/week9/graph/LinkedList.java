package kz.aitu.week9.graph;

public class LinkedList<Key, Value> {
    public Vertex head;
    public Vertex tail;

    public boolean contains(Vertex vertex) {
        Vertex currrent = head;
        while (currrent != null) {
            if (currrent == vertex) {
                return true;
            }
            currrent = currrent.getNextL();
        }
        return false;
    }

    public void add(Vertex vertex) {
        if (head == null) {
            head = vertex;
        } else {
            tail.setNextL(vertex);
        }
        tail = vertex;
    }

    public boolean connectedWith(Vertex vertex2) {
        if (head == null) {
            return false;
        } else {
            Vertex current = head;
            while (current != null) {
                if (current == vertex2) {
                    return true;
                }
                current = current.getNext();
            }
            return false;
        }
    }

    public boolean isEmpty() {
        if (head == null) {
            return true;
        } else {
            return false;
        }
    }


    public void setTail(Vertex tail) {
        this.tail = tail;
    }

    public void setHead(Vertex head) {
        this.head = head;
    }

    public Vertex getHead() {
        return head;
    }

    public Vertex getTail() {
        return tail;
    }
}
