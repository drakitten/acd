package kz.aitu.week9.dfs;

public class Stack {
    public Node top;
    public int size;

    public void setTop(Node top) {
        this.top = top;
    }

    public Node getTop() {
        return top;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void push(int data) {
        Node node = new Node(data);
        if (top == null) {
            top = node;
        } else {
            node.next = top;
            top = node;
        }
        size++;
    }
    public void remove() {
        top = top.next;
        size--;
    }
    public Node pop() {
        Node old = top;
        top = top.next;
        size--;
        return old;
    }
    public boolean isEmpty() {
        return this.top() == null;
    }
    public int size() {
        return this.size;
    }

    public Node top() {
        return this.top;
    }
}
