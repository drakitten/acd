package kz.aitu.week9.dfs;

public class Main {
    public static void dfs(BinaryTree t) {
        if (t.getRoot() == null) {
            return;
        }
        Stack s = new Stack();
        s.push(t.getRoot().getData());
        while (!s.isEmpty()){
            Node temp = s.pop();
            System.out.print(temp.getData()+" ");
            BinaryTreeNode b = t.find(temp.getData());
            if (b.getLeft() != null) {
                s.push(b.getLeft().getData());
            }
            if (b.getRight() != null) {
                s.push(b.getRight().getData());
            }
        }
    }
    public static void main(String[] args) {
        BinaryTree t = new BinaryTree();
        t.insert(1);
        t.insert(6);
        t.insert(3);
        t.insert(4);
        t.insert(5);

        dfs(t);
    }
}
