package kz.aitu.week9.dfs;

public class BinaryTree {
    public BinaryTreeNode root;

    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }

    public BinaryTreeNode getRoot() {
        return root;
    }

    public void insert(int data) {
        BinaryTreeNode b = new BinaryTreeNode(data);
        if (root == null) {
            root = b;
            return;
        }
        BinaryTreeNode parent = null;
        BinaryTreeNode current = root;
        while (true) {
            parent = current;
            if (data > current.getData()) {
                current = current.right;
                if (current == null) {
                    parent.right = b;
                    return;
                }
            } else {
                current = current.left;
                if (current == null) {
                    parent.left = b;
                    return;
                }
            }
        }
    }

    public void display(BinaryTreeNode root) {
        if (root != null) {
            display(root.left);
            System.out.println(root.data + " ");
            display(root.right);
        }
    }

    public BinaryTreeNode find(int data) {
        return findNode(this.root, data);
    }
    public BinaryTreeNode findNode(BinaryTreeNode b, int ser) {
        if (b == null) {
            return null;
        }
        if (b.data > ser) {
            return findNode(b.right, ser);
        } else if (b.data < ser) {
            return findNode(b.left, ser);
        } else {
            return b;
        }
    }
}
